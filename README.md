# KiraTech Frontend Assignment

Thanks to Kiratech for giving me the opportunity to proceed with this frontend assignment.

Below are quick documentation on the development
## Development
`Framework:` 
NuxtJS 2

`Vue Version:` 2.6

`UI: ` TailwindCSS + DaisyUI

`Plugins:`
- vue-fontawesome -> For Icon
- tailwindcss -> UI design
- daisyui -> Tailwind UI plugin
- vue-moment -> Date plugin
- axios -> HTTP library
 

## Running the application
```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
